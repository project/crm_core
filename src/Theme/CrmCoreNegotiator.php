<?php

namespace Drupal\crm_core\Theme;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Theme\ThemeNegotiatorInterface;

/**
 * Provides the theme negotiator to use on crm-core pages.
 */
class CrmCoreNegotiator implements ThemeNegotiatorInterface {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Creates a new Theme Negotiator instance.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function determineActiveTheme(RouteMatchInterface $route_match) {
    return $this->configFactory->get('crm_core.settings')->get('custom_theme') ?: NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {
    if ($this->configFactory->get('crm_core.settings')->get('custom_theme')) {
      if ($route_match->getRouteObject()) {
        $path = $route_match->getRouteObject()->getPath();
        if (strpos($path, '/crm-core') === 0) {
          return TRUE;
        }
      }
    }
    return FALSE;
  }

}
