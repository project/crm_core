<?php

namespace Drupal\Tests\crm_core_user_sync\Kernel;

use Drupal\Core\Language\LanguageInterface;
use Drupal\KernelTests\KernelTestBase;

/**
 * Tests the admin listing fallback when views is not enabled.
 *
 * @group crm_core_user_sync
 */
class CrmCoreUserSyncRelationListBuilderTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'name',
    'crm_core_contact',
    'crm_core_user_sync',
    'user',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('crm_core_individual');
    $this->installEntitySchema('crm_core_user_sync_relation');
  }

  /**
   * Tests that the correct cache contexts are set.
   */
  public function testCacheContexts() {
    /** @var \Drupal\Core\Entity\EntityListBuilderInterface $list_builder */
    $list_builder = $this->container->get('entity_type.manager')
      ->getListBuilder('crm_core_user_sync_relation');

    $build = $list_builder->render();
    $this->container->get('renderer')->renderRoot($build);

    $this->assertEquals([
      'languages:' . LanguageInterface::TYPE_INTERFACE,
      'theme',
      'user.permissions',
      'url.query_args.pagers:0',
    ],
      $build['#cache']['contexts']
    );
  }

}
