<?php

namespace Drupal\Tests\crm_core_user_sync\Functional;

use Drupal\crm_core_contact\Entity\Individual;

/**
 * Tests the listing of crm contact user relation.
 *
 * Tests the fallback relation list when Views is disabled.
 *
 * @group crm_core_user_sync
 * @see \Drupal\crm_core_user_sync\RelationListBuilder
 */
class RelationListTest extends RelationTestBase {

  /**
   * Tests the crm user relation listing page.
   */
  public function testListing() {
    $this->drupalLogin($this->drupalCreateUser([
      'administer crm-core-user-sync',
      'translate configuration',
    ]));
    $this->drupalGet('admin/config/crm-core/user-sync/relation');

    // Test for the page title.
    $this->assertSession()->titleEquals('Relations | Drupal');

    // Test for the table.
    $element = $this->xpath('//div[@class="layout-content"]//table');
    $this->assertNotEmpty($element, 'Configuration entity list table found.');

    // Test the table header.
    $elements = $this->xpath('//div[@class="layout-content"]//table/thead/tr/th');
    $this->assertEquals(count($elements), 4, 'Correct number of table header cells found.');

    // Test the contents of each th cell.
    $expected_items = ['ID', 'User', 'Individual', 'Operations'];
    foreach ($elements as $key => $element) {
      $this->assertEquals($element->getText(), $expected_items[$key]);
    }

    // Add a new entity using the operations link.
    $link_text = 'Add relation';
    $this->assertSession()->linkExists($link_text);
    $this->clickLink($link_text);
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals('admin/config/crm-core/user-sync/relation/add');

    $edit = [];
    $edit['user_id[0][target_id]'] = 'adminUser (1)';
    $edit['individual_id[0][target_id]'] = 'adminContact (1)';

    $this->submitForm($edit, 'Save');

    $this->assertSession()->addressEquals('admin/config/crm-core/user-sync/relation');

    // Confirm that once the user returns to the listing, the text of the user
    // and contact (versus elsewhere on the page).
    $id = $this->xpath('//td[1]');
    $this->assertEquals($id[0]->getText(), '1', 'The Id of the relation.');
    $user = $this->xpath('//td[2]');
    $this->assertEquals($user[0]->getText(), 'admin', 'The user of the relation.');

    // Check the number of table row cells.
    $elements = $this->xpath('//div[@class="layout-content"]//table/tbody/tr/td');
    $this->assertEquals(count($elements), 4, 'Correct number of table row cells found.');
    // Check the contents of each row cell. The first cell contains the label,
    // the second contains the machine name, and the third contains the
    // operations list.
    $this->assertEquals($elements[0]->getText(), '1');
    $this->assertEquals($elements[1]->getText(), 'admin');
    $this->assertTrue(strpos($elements[2]->getText(), 'Nameless Person') !== FALSE);
    $this->assertEquals($elements[3]->getText(), 'EditDelete');

    $another_contact = Individual::create(['type' => 'person']);
    $another_contact->save();

    // Edit the entity using the operations link.
    $relations = $this->container
      ->get('entity_type.manager')
      ->getStorage('crm_core_user_sync_relation')
      ->loadByProperties(['user_id' => 1]);
    $relation = reset($relations);
    if (!empty($relation)) {
      $this->assertSession()->linkByHrefExists('admin/config/crm-core/user-sync/relation/' . $relation->id());
      $this->clickLink('Edit');
      $this->assertSession()->statusCodeEquals(200);
      $this->assertSession()->titleEquals('Edit | Drupal');
      $edit = ['individual_id[0][target_id]' => 'admin (' . $another_contact->id() . ')'];
      $this->submitForm($edit, 'Save');
    }
    else {
      $this->fail('Did not find relation in the database.');
    }

    // Confirm that once the user returns to the listing, the text of the label
    // (versus elsewhere on the page).
    $admin = $this->xpath('//td[2]');
    $this->assertEquals($admin[0]->getText(), 'admin', 'The user of the relation.');

    // Delete the added entity using the operations link.
    $this->assertSession()->linkByHrefExists('admin/config/crm-core/user-sync/relation/' . $relation->id() . '/delete');
    $delete_text = 'Delete';
    $this->clickLink($delete_text);
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->titleEquals('Are you sure you want to delete the relation 1? | Drupal');
    $this->submitForm([], $delete_text);

    // Confirm that the empty text is displayed.
    $this->assertSession()->pageTextContains('There are no relation entities yet.');
  }

}
