<?php

namespace Drupal\Tests\crm_core_user_sync\Functional;

use Drupal\crm_core_contact\Entity\Individual;
use Drupal\crm_core_contact\Entity\IndividualType;
use Drupal\Tests\BrowserTestBase;

/**
 * Sets up user contact relationship.
 */
abstract class RelationTestBase extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Admin user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * Contact for the admin user.
   *
   * @var \Drupal\crm_core_contact\Entity\IndividualInterface
   */
  protected $adminContact;

  /**
   * Permissions to grant admin user.
   *
   * @var array
   */
  protected $permissions = [
    'administer crm-core-user-sync',
  ];

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'name',
    'crm_core_contact',
    'block',
    'views_ui',
    'options',
    'datetime',
    'crm_core_user_sync',
    'config_translation',
  ];

  /**
   * Sets the test up.
   */
  protected function setUp(): void {
    parent::setUp();

    $this->adminUser = $this->drupalCreateUser($this->permissions, 'adminUser');
    IndividualType::create([
      'name' => 'Person',
      'type' => 'person',
      'description' => 'A generic person Individual type to relate to users.',
      'primary_fields' => [],
    ])->save();

    $this->adminContact = Individual::create(['type' => 'person']);
    $this->adminContact->save();
    // Place local actions and local task blocks.
    $this->drupalPlaceBlock('local_actions_block');
    $this->drupalPlaceBlock('local_tasks_block');

  }

}
