<?php

namespace Drupal\crm_core_user_sync\Plugin\views\query;

use Drupal\views\Plugin\views\query\QueryPluginBase;

/**
 * The.
 *
 * @ViewsQuery(
 *   id = "crm_core_user_sync",
 *   title = @Translation("User Contact"),
 *   help = @Translation("Relationship between the user and Individual.")
 * )
 */
class Relation extends QueryPluginBase {

  /**
   * {@inheritdoc}
   */
  public function ensureTable($table, $relationship = NULL) {
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function addField($table, $field, $alias = '', $params = []) {
    return $field;
  }

}
