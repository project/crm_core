<?php

namespace Drupal\crm_core_user_sync\Plugin\views\field;

use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Contact User Relation field plugin.
 *
 * @ViewsField("crm_core_user_contact")
 */
class Relation extends FieldPluginBase {

  /**
   * The crm core user sync relation service.
   *
   * @var \Drupal\crm_core_user_sync\CrmCoreUserSyncRelation
   */
  protected $service;

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Leave empty to avoid a query on this field.
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {

    $this->service = \Drupal::service('crm_core_user_sync.relation');
    $entity = $values->_entity;

    $entity_type = $entity->getEntityTypeId();
    $entity_id = $entity->id();

    $current_path = \Drupal::service('path.current')->getPath();
    if ($entity_type == 'crm_core_individual') {
      $uid = $this->service->getUserIdFromIndividualId($entity_id);
      if (empty($uid)) {
        $url = Url::fromRoute(
          'entity.crm_core_user_sync_relation.add_form',
          ['individual_id' => $entity_id],
          [
            'query' => [
              'destination' => Url::fromUri("internal:$current_path")->toString(),
            ],
          ]
        );
        $link = Link::fromTextAndUrl('Add Relation', $url)->toString();
      }
      else {
        $rid = $this->service->getRelationIdFromIndividualId($entity_id);
        $url = Url::fromRoute(
          'entity.crm_core_user_sync_relation.edit_form',
          ['crm_core_user_sync_relation' => $rid],
          [
            'query' => [
              'destination' => Url::fromUri("internal:$current_path")->toString(),
            ],
          ]
        );
        $link = Link::fromTextAndUrl($rid, $url)->toString();
      }

    }
    elseif ($entity_type == 'user') {
      $individual_id = $this->service->getIndividualIdFromUserId($entity_id);
      if (empty($individual_id)) {
        $url = Url::fromRoute(
          'entity.crm_core_user_sync_relation.add_form',
          ['user_id' => $entity_id],
          [
            'query' => [
              'destination' => Url::fromUri("internal:$current_path")->toString(),
            ],
          ]
        );
        $link = Link::fromTextAndUrl('Add Relation', $url)->toString();
      }
      else {
        $rid = $this->service->getRelationIdFromUserId($entity_id);
        $url = Url::fromRoute(
          'entity.crm_core_user_sync_relation.edit_form',
          ['crm_core_user_sync_relation' => $rid],
          [
            'query' => [
              'destination' => Url::fromUri("internal:$current_path")->toString(),
            ],
          ]
        );
        $link = Link::fromTextAndUrl($rid, $url)->toString();
      }
    }

    return $link;
  }

}
