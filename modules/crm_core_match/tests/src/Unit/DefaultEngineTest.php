<?php

namespace Drupal\Tests\crm_core_match\Unit;

use Drupal\crm_core_match\Plugin\crm_core_match\engine\DefaultMatchingEngine;
use Drupal\Tests\UnitTestCase;

/**
 * Tests the default matching engine.
 *
 * @coversDefaultClass \Drupal\crm_core_match\Plugin\crm_core_match\engine\DefaultMatchingEngine
 *
 * @group crm_core
 */
class DefaultEngineTest extends UnitTestCase {

  /**
   * The mocked match field plugin manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $pluginManager;


  /**
   * The mocked entity manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $entityTypeManager;

  /**
   * The entity field manager service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * A mocked individual entity used to get matches.
   *
   * @var \Drupal\crm_core_contact\Entity\Individual|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $individual;

  /**
   * A mocked matcher.
   *
   * @var \Drupal\crm_core_match\Entity\Matcher|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $matcher;

  /**
   * The tested engine.
   *
   * @var \Drupal\crm_core_match\Plugin\crm_core_match\engine\DefaultMatchingEngine
   */
  protected $engine;

  /**
   * A mocked field definition.
   *
   * @var \Drupal\Core\Field\FieldDefinitionInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $field;

  /**
   * A mocked match field handler.
   *
   * @var \Drupal\crm_core_match\Plugin\crm_core_match\field\FieldHandlerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $matchHandler;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->pluginManager = $this->createMock('\Drupal\Core\Entity\EntityTypeManagerInterface');

    $this->entityTypeManager = $this->createMock('\Drupal\Core\Entity\EntityTypeManagerInterface');
    $this->entityFieldManager = $this->createMock('\Drupal\Core\Entity\EntityFieldManagerInterface');
    $this->individual = $this->createMock('Drupal\crm_core_contact\Entity\Individual');

    $this->individual->expects($this->any())
      ->method('bundle')
      ->willReturn('dogs');

    $this->matcher = $this->createMock('Drupal\crm_core_match\Entity\Matcher');
    $this->matcher->expects($this->any())
      ->method('status')
      ->willReturn(TRUE);

    $storage = $this->createMock('Drupal\Core\Entity\EntityStorageInterface');
    $storage->expects($this->any())
      ->method('load')
      ->with('dogs')
      ->willReturn($this->matcher);

    $this->entityTypeManager->expects($this->any())
      ->method('getStorage')
      ->willReturn($storage);

    $this->field = $this->createMock('Drupal\Core\Field\FieldDefinitionInterface');

    $this->matchHandler = $this->createMock('Drupal\crm_core_match\Plugin\crm_core_match\field\FieldHandlerInterface');
    $this->matchHandler->expects($this->any())
      ->method('getPropertyNames')
      ->willReturn(['value']);

    $this->engine = new DefaultMatchingEngine([
      'rules' => ['foo' => [], 'bar' => []],
      'threshold' => 50,
    ], 'default', [], $this->pluginManager, $this->entityTypeManager, $this->entityFieldManager);
  }

  /**
   * Tests the match method.
   */
  public function testMatch() {
    $this->field->expects($this->any())
      ->method('getType')
      ->willReturn('crap');

    $this->individual->expects($this->any())
      ->method('getFieldDefinitions')
      ->willReturn([
        'foo' => $this->field,
      ]);

    $this->pluginManager->expects($this->any())
      ->method('hasDefinition')
      ->willReturn(TRUE);

    $this->matchHandler->expects($this->any())
      ->method('match')
      ->willReturn([
        '42' => [
          'value' => 100,
        ],
      ]);

    $this->pluginManager->expects($this->once())
      ->method('createInstance')
      ->willReturn($this->matchHandler);

    $ids = $this->engine->match($this->individual);
    $this->assertEquals([42], $ids);
  }

  /**
   * Tests the match method with multiple fields.
   */
  public function testMultipleMatch() {
    $this->field->expects($this->any())
      ->method('getType')
      ->willReturn('crap');

    $this->individual->expects($this->any())
      ->method('getFieldDefinitions')
      ->willReturn([
        'foo' => $this->field,
        'bar' => $this->field,
      ]);

    $this->pluginManager->expects($this->any())
      ->method('hasDefinition')
      ->willReturn(TRUE);

    $this->matchHandler->expects($this->any())
      ->method('match')
      ->willReturn([
        '42' => [
          'value' => 40,
        ],
        '30' => [
          'value' => 40,
        ],
      ]);

    $this->pluginManager->expects($this->any())
      ->method('createInstance')
      ->willReturn($this->matchHandler);

    $ids = $this->engine->match($this->individual);
    $this->assertEquals([42], $ids);
  }

}
