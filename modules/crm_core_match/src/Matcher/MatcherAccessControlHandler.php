<?php

namespace Drupal\crm_core_match\Matcher;

use Drupal\Core\Entity\EntityAccessControlHandler;

/**
 * Controls access to matchers.
 */
class MatcherAccessControlHandler extends EntityAccessControlHandler {


}
