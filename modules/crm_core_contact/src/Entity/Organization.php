<?php

namespace Drupal\crm_core_contact\Entity;

use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\crm_core_contact\OrganizationInterface;
use Drupal\entity\Revision\RevisionableContentEntityBase;
use Drupal\user\EntityOwnerTrait;

/**
 * CRM Organization Entity Class.
 *
 * @ContentEntityType(
 *   id = "crm_core_organization",
 *   label = @Translation("Organization"),
 *   bundle_label = @Translation("Organization type"),
 *   handlers = {
 *     "access" = "Drupal\crm_core_contact\OrganizationAccessControlHandler",
 *     "form" = {
 *       "default" = "Drupal\crm_core_contact\Form\OrganizationForm",
 *       "delete" = "Drupal\crm_core_contact\Form\ContactDeleteForm",
 *     },
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\crm_core_contact\OrganizationListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *       "revision" = "\Drupal\entity\Routing\RevisionRouteProvider",
 *     },
 *     "local_task_provider" = {
 *       "default" = "\Drupal\crm_core_contact\Menu\ContactLocalTaskProvider",
 *     },
 *   },
 *   base_table = "crm_core_organization",
 *   revision_table = "crm_core_organization_revision",
 *   admin_permission = "administer crm_core_organization entities",
 *   show_revision_ui = TRUE,
 *   entity_keys = {
 *     "id" = "organization_id",
 *     "revision" = "revision_id",
 *     "bundle" = "type",
 *     "uuid" = "uuid",
 *     "label" = "name",
 *     "langcode" = "langcode",
 *     "owner" = "uid",
 *     "published" = "status",
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_user",
 *     "revision_created" = "revision_created",
 *     "revision_log_message" = "revision_log_message",
 *   },
 *   bundle_entity_type = "crm_core_organization_type",
 *   field_ui_base_route = "entity.crm_core_organization_type.edit_form",
 *   permission_granularity = "bundle",
 *   permission_labels = {
 *     "singular" = @Translation("Organization"),
 *     "plural" = @Translation("Organizations"),
 *   },
 *   links = {
 *     "add-page" = "/crm-core/organization/add",
 *     "add-form" = "/crm-core/organization/add/{crm_core_organization_type}",
 *     "canonical" = "/crm-core/organization/{crm_core_organization}",
 *     "collection" = "/crm-core/organization",
 *     "edit-form" = "/crm-core/organization/{crm_core_organization}/edit",
 *     "delete-form" = "/crm-core/organization/{crm_core_organization}/delete",
 *     "revision" = "/crm-core/organization/{crm_core_organization}/revisions/{crm_core_organization_revision}/view",
 *     "revision-revert-form" = "/crm-core/organization/{crm_core_organization}/revisions/{crm_core_organization_revision}/revert",
 *     "version-history" = "/crm-core/organization/{crm_core_organization}/revisions",
 *   }
 * )
 */
class Organization extends RevisionableContentEntityBase implements EntityPublishedInterface, OrganizationInterface {

  use EntityChangedTrait;
  use EntityOwnerTrait;
  use EntityPublishedTrait;
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields += static::ownerBaseFieldDefinitions($entity_type);
    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['uid']
      ->setLabel(t('Owned by'))
      ->setDescription(t('The username of the contact owner.'))
      ->setRevisionable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['status']->setLabel(t('Active'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => 1,
        'settings' => [
          'display_label' => TRUE,
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the organization was created.'))
      ->setRevisionable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the organization was last edited.'))
      ->setRevisionable(TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setRevisionable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'text_textfield',
        'weight' => 0,
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getPrimaryAddress() {
    return $this->getPrimaryField('address');
  }

  /**
   * {@inheritdoc}
   */
  public function getPrimaryEmail() {
    return $this->getPrimaryField('email');
  }

  /**
   * {@inheritdoc}
   */
  public function getPrimaryPhone() {
    return $this->getPrimaryField('phone');
  }

  /**
   * Gets the primary field.
   *
   * @param string $field
   *   The primary field name.
   *
   * @return \Drupal\Core\Field\FieldItemListInterface|\Drupal\Core\TypedData\TypedDataInterface
   *   The primary field property object.
   *
   * @throws \InvalidArgumentException
   *   If no primary field is configured.
   *   If the configured primary field does not exist.
   */
  public function getPrimaryField($field) {
    $type = $this->get('type')->entity;
    $name = empty($type->getPrimaryFields()[$field]) ? '' : $type->getPrimaryFields()[$field];
    return $this->get($name);
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    $label = $this->get('name')->value;
    if (empty($label)) {
      $label = $this->t('Nameless #@id', ['@id' => $this->id()]);
    }
    \Drupal::moduleHandler()->alter('crm_core_organization_label', $label, $this);

    return $label;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    foreach (array_keys($this->getTranslationLanguages()) as $langcode) {
      $translation = $this->getTranslation($langcode);

      // If no owner has been set explicitly, make the anonymous user the owner.
      if (!$translation->getOwner()) {
        $translation->setOwnerId(0);
      }
    }

    // If no revision owner has been set explicitly, make the organization owner
    // the revision owner.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }
  }

}
