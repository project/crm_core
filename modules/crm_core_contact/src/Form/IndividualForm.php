<?php

namespace Drupal\crm_core_contact\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Session\AccountInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for the Individual entity.
 */
class IndividualForm extends ContentEntityForm {

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The logger channel.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The Current User object.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Constructs a IndividualForm object.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface|null $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface|null $time
   *   The time service.
   */
  public function __construct(
    EntityRepositoryInterface $entity_repository,
    AccountInterface $current_user,
    DateFormatterInterface $date_formatter,
    MessengerInterface $messenger,
    LoggerInterface $logger,
    ?EntityTypeBundleInfoInterface $entity_type_bundle_info = NULL,
    ?TimeInterface $time = NULL,
  ) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);
    $this->currentUser = $current_user;
    $this->dateFormatter = $date_formatter;
    $this->messenger = $messenger;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('current_user'),
      $container->get('date.formatter'),
      $container->get('messenger'),
      $container->get('logger.channel.crm_core_contact'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    /** @var \Drupal\core_core_contact\IndividualInterface $contact */
    $contact = $this->entity;

    $form['advanced']['#attributes']['class'][] = 'entity-meta';

    $form['meta'] = [
      '#type' => 'details',
      '#group' => 'advanced',
      '#weight' => -10,
      '#title' => $this->t('Status'),
      '#attributes' => ['class' => ['entity-meta__header']],
      '#tree' => TRUE,
      '#access' => $this->currentUser->hasPermission('administer crm_core_individual entities'),
    ];
    $form['meta']['published'] = [
      '#type' => 'item',
      '#markup' => $contact->isPublished() ? $this->t('Active') : $this->t('Inactive'),
      '#access' => !$contact->isNew(),
      '#wrapper_attributes' => ['class' => ['entity-meta__title']],
    ];
    $form['meta']['changed'] = [
      '#type' => 'item',
      '#title' => $this->t('Last saved'),
      '#markup' => !$contact->isNew() ? $this->dateFormatter->format($contact->getChangedTime(), 'short') : $this->t('Not saved yet'),
      '#wrapper_attributes' => ['class' => ['entity-meta__last-saved']],
    ];
    if ($contact->getOwner()) {
      $form['meta']['author'] = [
        '#type' => 'item',
        '#title' => $this->t('Owner'),
        '#markup' => $contact->getOwner()?->getAccountName(),
        '#wrapper_attributes' => ['class' => ['entity-meta__author']],
      ];
    }

    $form['status']['#group'] = 'footer';

    // Contact owner information for administrators.
    $form['author'] = [
      '#type' => 'details',
      '#title' => $this->t('Owner information'),
      '#group' => 'advanced',
      '#attributes' => [
        'class' => ['contact-form-owner'],
      ],
      '#attached' => [
        'library' => ['crm_core_contact/crm_core_contact.contact'],
      ],
      '#weight' => 90,
      '#optional' => TRUE,
      '#access' => $this->currentUser->hasPermission('administer crm_core_individual entities'),
    ];

    if (isset($form['uid'])) {
      $form['uid']['#group'] = 'author';
    }

    if (isset($form['created'])) {
      $form['created']['#group'] = 'author';
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $individual = $this->entity;

    $status = $individual->save();

    $args = [
      '%name' => $individual->label(),
      'link' => $individual->toLink()->toString(),
    ];

    if ($status == SAVED_UPDATED) {
      $this->messenger->addMessage($this->t('The individual %name has been updated.', $args));
    }
    elseif ($status == SAVED_NEW) {
      $this->messenger->addMessage($this->t('The individual %name has been added.', $args));
      $this->logger->notice('Added individual %name.', $args);
    }

    if ($individual->access('view')) {
      $form_state->setRedirect('entity.crm_core_individual.canonical', ['crm_core_individual' => $individual->id()]);
    }
    else {
      $form_state->setRedirect('entity.crm_core_individual.collection');
    }

    return $status;
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $actions = parent::actions($form, $form_state);
    $actions['submit']['#value'] = $this->t('Save @individual_type', [
      '@individual_type' => $this->entity->get('type')->entity->label(),
    ]);
    return $actions;
  }

}
