<?php

namespace Drupal\crm_core_contact\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * The confirmation form for deleting a contact.
 */
class ContactDeleteForm extends ContentEntityDeleteForm {

  /**
   * {@inheritdoc}
   */
  protected function getDeletionMessage() {
    $entity = $this->getEntity();
    $entity_type = $entity->bundle();

    if (!$entity->isDefaultTranslation()) {
      return $this->t('@language translation of the @type %label has been deleted.', [
        '@language' => $entity->language()->getName(),
        '@type' => $entity_type,
        '%label' => $entity->label(),
      ]);
    }

    return $this->t('The @type %name has been deleted.', [
      '@type' => $entity_type,
      '%name' => $entity->label(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  protected function logDeletionMessage() {
    $entity = $this->getEntity();
    $this->logger('crm_core_contact')->notice('@type: deleted %name (id:%id).', [
      '@type' => $entity->bundle(),
      '%name' => $entity->label(),
      '%id' => $entity->id(),
    ]);
  }

}
