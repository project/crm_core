<?php

namespace Drupal\Tests\crm_core_contact\Unit;

use Drupal\crm_core_contact\Entity\Individual;
use Drupal\crm_core_contact\Entity\Organization;
use Drupal\Tests\UnitTestCase;

/**
 * Test the module file.
 *
 * @group crm_core
 */
class ContactModuleTest extends UnitTestCase {

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    require_once __DIR__ . '/../../../crm_core_contact.module';
  }

  /**
   * Test suggestions.
   *
   * @covers ::crm_core_contact_theme_suggestions_crm_core_individual
   */
  public function testIndividualSuggestions(): void {
    $individual = $this->createMock(Individual::class);
    $individual
      ->method('bundle')->willReturn('customer');
    $individual
      ->method('id')->willReturn(1);
    $result = crm_core_contact_theme_suggestions_crm_core_individual(
      [
        'elements' => [
          '#crm_core_individual' => $individual,
          '#view_mode' => 'my.test',
        ],
      ]
    );
    $this->assertEquals($result[0], 'crm_core_individual__my_test');
    $this->assertEquals($result[1], 'crm_core_individual__customer');
    $this->assertEquals($result[2], 'crm_core_individual__customer__my_test');
    $this->assertEquals($result[3], 'crm_core_individual__1');
    $this->assertEquals($result[4], 'crm_core_individual__1__my_test');
  }

  /**
   * Test suggestions.
   *
   * @covers ::crm_core_contact_theme_suggestions_crm_core_organization
   */
  public function testOrganizationSuggestions(): void {
    $organization = $this->createMock(Organization::class);
    $organization
      ->method('bundle')->willReturn('customer');
    $organization
      ->method('id')->willReturn(1);
    $result = crm_core_contact_theme_suggestions_crm_core_organization(
      [
        'elements' => [
          '#crm_core_organization' => $organization,
          '#view_mode' => 'my.test',
        ],
      ]
    );
    $this->assertEquals($result[0], 'crm_core_organization__my_test');
    $this->assertEquals($result[1], 'crm_core_organization__customer');
    $this->assertEquals($result[2], 'crm_core_organization__customer__my_test');
    $this->assertEquals($result[3], 'crm_core_organization__1');
    $this->assertEquals($result[4], 'crm_core_organization__1__my_test');
  }

  /**
   * Test template hook.
   *
   * @covers ::crm_core_contact_theme
   */
  public function testTemplate(): void {
    $templates = crm_core_contact_theme();
    $this->assertEquals('crm-core-organization', $templates['crm_core_organization']['template']);
    $this->assertEquals('crm-core-individual', $templates['crm_core_individual']['template']);
  }

  /**
   * Test mail hook.
   *
   * @covers ::crm_core_contact_mail
   */
  public function testMail(): void {
    $params = [
      'subject' => 'Subject',
      'message' => 'Content',
    ];
    $message = [];
    crm_core_contact_mail('example', $message, $params);
    $this->assertEquals($message['subject'], $params['subject']);
    $this->assertEquals($message['body'][0], $params['message']);
  }

}
