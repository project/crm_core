<?php

namespace Drupal\Tests\crm_core_contact\Functional;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Session\AccountInterface;
use Drupal\crm_core_contact\OrganizationInterface;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\crm_core_contact\Traits\IndividualCreationTrait;
use Drupal\Tests\crm_core_contact\Traits\IndividualTypeCreationTrait;
use Drupal\Tests\crm_core_contact\Traits\OrganizationCreationTrait;
use Drupal\Tests\crm_core_contact\Traits\OrganizationTypeCreationTrait;

/**
 * Sets up page and article content types.
 */
abstract class ContactTestBase extends BrowserTestBase {

  use OrganizationCreationTrait {
    getOrganizationByName as drupalGetOrganizationByName;
    createOrganization as drupalCreateOrganization;
  }

  use OrganizationTypeCreationTrait {
    createOrganizationType as drupalCreateOrganizationType;
  }

  use IndividualCreationTrait {
    getIndividualByLabel as drupalGetIndividualByLabel;
    createIndividual as drupalCreateIndividual;
  }

  use IndividualTypeCreationTrait {
    createIndividualType as drupalCreateIndividualType;
  }

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['crm_core_contact', 'datetime'];

  /**
   * The organization access control handler.
   *
   * @var \Drupal\Core\Entity\EntityAccessControlHandlerInterface
   */
  protected $organizationAccessHandler;

  /**
   * The individual access control handler.
   *
   * @var \Drupal\Core\Entity\EntityAccessControlHandlerInterface
   */
  protected $individualAccessHandler;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->drupalCreateOrganizationType([
      'type' => 'company',
      'name' => 'Company',
    ]);
    $this->organizationAccessHandler = \Drupal::entityTypeManager()->getAccessControlHandler('crm_core_organization');

    $this->drupalCreateIndividualType([
      'type' => 'person',
      'name' => 'Person',
    ]);
    $this->individualAccessHandler = \Drupal::entityTypeManager()->getAccessControlHandler('crm_core_individual');
  }

  /**
   * Asserts that contact access correctly grants or denies access.
   *
   * @param array $ops
   *   An associative array of the expected organization access grants for the
   *   organization and account, with each key as the name of an operation
   *   (e.g. 'view', 'delete') and each value a Boolean indicating whether
   *   access to that operation should be granted.
   * @param \Drupal\crm_core_contact\OrganizationInterface $organization
   *   The contact object to check.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user account for which to check access.
   *
   * @internal
   */
  public function assertOrganizationAccess(array $ops, OrganizationInterface $organization, AccountInterface $account) {
    foreach ($ops as $op => $result) {
      $this->assertEquals($this->organizationAccessHandler->access($organization, $op, $account), $result, $this->organizationAccessAssertMessage($op, $result, $organization->language()->getId()));
    }
  }

  /**
   * Asserts that organization create access correctly grants or denies access.
   *
   * @param string $bundle
   *   The organization bundle to check access to.
   * @param bool $result
   *   Whether access should be granted or not.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user account for which to check access.
   * @param string|null $langcode
   *   (optional) The language code indicating which translation of the
   *   organization to check. If NULL, the untranslated (fallback) access is
   *   checked.
   *
   * @internal
   */
  public function assertOrganizationCreateAccess(string $bundle, bool $result, AccountInterface $account, ?string $langcode = NULL) {
    $this->assertEquals($this->organizationAccessHandler->createAccess($bundle, $account, ['langcode' => $langcode]), $result, $this->organizationAccessAssertMessage('create', $result, $langcode));
  }

  /**
   * Constructs an assert message to display the tested organization.
   *
   * @param string $operation
   *   The operation to check access for.
   * @param bool $result
   *   Whether access should be granted or not.
   * @param string|null $langcode
   *   (optional) The language code indicating which translation of the
   *   organization to check. If NULL, the untranslated (fallback) access
   *   is checked.
   *
   * @return string
   *   An assert message string which contains information in plain English
   *   about the organization access permission test that was performed.
   */
  public function organizationAccessAssertMessage($operation, $result, $langcode = NULL) {
    return new FormattableMarkup(
      'Organization access returns @result with operation %op, language code %langcode.',
      [
        '@result' => $result ? 'true' : 'false',
        '%op' => $operation,
        '%langcode' => !empty($langcode) ? $langcode : 'empty',
      ]
    );
  }

  /**
   * Asserts that contact access correctly grants or denies access.
   *
   * @param array $ops
   *   An associative array of the expected organization access grants for the
   *   organization and account, with each key as the name of an operation
   *   (e.g. 'view', 'delete') and each value a Boolean indicating whether
   *   access to that operation should be granted.
   * @param \Drupal\crm_core_contact\IndividualInterface $individual
   *   The contact object to check.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user account for which to check access.
   *
   * @internal
   */
  public function assertIndividualAccess(array $ops, IndividualInterface $individual, AccountInterface $account) {
    foreach ($ops as $op => $result) {
      $this->assertEquals($this->individualAccessHandler->access($individual, $op, $account), $result, $this->individualAccessAssertMessage($op, $result, $individual->language()->getId()));
    }
  }

  /**
   * Asserts that organization create access correctly grants or denies access.
   *
   * @param string $bundle
   *   The organization bundle to check access to.
   * @param bool $result
   *   Whether access should be granted or not.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user account for which to check access.
   * @param string|null $langcode
   *   (optional) The language code indicating which translation of the
   *   organization to check. If NULL, the untranslated (fallback) access is
   *   checked.
   *
   * @internal
   */
  public function assertIndividualCreateAccess(string $bundle, bool $result, AccountInterface $account, ?string $langcode = NULL) {
    $this->assertEquals($this->individualAccessHandler->createAccess($bundle, $account, ['langcode' => $langcode]), $result, $this->individualAccessAssertMessage('create', $result, $langcode));
  }

  /**
   * Constructs an assert message to display the tested individual.
   *
   * @param string $operation
   *   The operation to check access for.
   * @param bool $result
   *   Whether access should be granted or not.
   * @param string|null $langcode
   *   (optional) The language code indicating which translation of the
   *   individual to check. If NULL, the untranslated (fallback) access
   *   is checked.
   *
   * @return string
   *   An assert message string which contains information in plain English
   *   about the organization access permission test that was performed.
   */
  public function individualAccessAssertMessage($operation, $result, $langcode = NULL) {
    return new FormattableMarkup(
      'Individual access returns @result with operation %op, language code %langcode.',
      [
        '@result' => $result ? 'true' : 'false',
        '%op' => $operation,
        '%langcode' => !empty($langcode) ? $langcode : 'empty',
      ]
    );
  }

}
