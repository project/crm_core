<?php

namespace Drupal\Tests\crm_core_contact\Functional;

use Drupal\crm_core_contact\Entity\Organization;
use Drupal\crm_core_contact\Entity\OrganizationType;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests the UI for Organization CRUD operations.
 *
 * @group crm_core_contact
 */
class OrganizationUiTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'crm_core_contact',
    'crm_core_activity',
    'block',
    'datetime',
    'options',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    OrganizationType::create([
      'label' => 'Supplier',
      'id' => 'supplier',
      'description' => 'A person or company that supplies goods or services.',
      'primary_fields' => [],
    ])->save();

    OrganizationType::create([
      'label' => 'Household',
      'id' => 'household',
      'description' => 'A collection of individuals generally located at the same residence.',
      'primary_fields' => [],
    ])->save();

    // Place local actions and local task blocks.
    $this->drupalPlaceBlock('local_actions_block');
    $this->drupalPlaceBlock('local_tasks_block');
  }

  /**
   * Tests the organization operations.
   *
   * User with permissions 'administer crm_core_organization entities' should
   * be able to create/edit/delete organizations of any organization type.
   */
  public function testOrganizationOperations() {
    // Create user and login.
    $user = $this->drupalCreateUser([
      'administer crm_core_organization entities',
      'administer organization types',
      'create crm_core_organization entities of bundle supplier',
      'view any crm_core_organization entity',
      'view any crm_core_activity entity',
    ]);
    $this->drupalLogin($user);

    $this->drupalGet('crm-core');
    $this->assertSession()->linkExists('CRM Organizations');
    $this->clickLink('CRM Organizations');
    // There should be no organizations available after fresh installation and
    // there is a link to create new organizations.
    $this->assertSession()->pageTextContains('There are no organizations available.');
    $this->assertSession()->linkExists('Add an organization');

    $household_values = [
      'name[0][value]' => 'Fam. Johnson',
      'status[value]' => TRUE,
    ];
    $this->drupalGet('crm-core/organization/add/household');
    $this->submitForm($household_values, 'Save Household');

    // Assert we were redirected back to the list of contacts.
    $this->assertSession()->addressEquals('crm-core/organization/1');

    $this->drupalGet('crm-core/organization');
    $this->assertSession()->pageTextContains('Fam. Johnson');
    $this->assertSession()->pageTextContains('Household');

    $households = \Drupal::entityTypeManager()
      ->getStorage('crm_core_organization')
      ->loadByProperties(['name' => 'Fam. Johnson']);
    $household = current($households);

    $household_values = [
      'name[0][value]' => 'Fam. Bane',
      'status[value]' => TRUE,
    ];
    $this->drupalGet('crm-core/organization/' . $household->id() . '/edit');
    $this->submitForm($household_values, 'Save Household');

    // Assert we are viewing the entity.
    $this->assertSession()->addressEquals('crm-core/organization/' . $household->id());
    $this->assertSession()->pageTextContains('Fam. Bane');

    // Check listing page.
    $this->drupalGet('crm-core/organization');
    $this->assertSession()->pageTextContains('Fam. Bane');

    // Create Supplier organization.
    $supplier_values = [
      'name[0][value]' => 'Example ltd',
      'status[value]' => TRUE,
    ];
    $this->drupalGet('crm-core/organization/add/supplier');
    $this->submitForm($supplier_values, 'Save Supplier');

    // Create supplier with no name.
    $this->drupalGet('crm-core/organization/add/supplier');
    $this->submitForm([], 'Save Supplier');

    // Assert we were redirected back to the list of organizations.
    $this->assertSession()->addressEquals('crm-core/organization/3');

    $this->drupalGet('crm-core/organization');
    $this->assertSession()->linkExists('Example ltd');
    $this->assertSession()->linkExists('Example ltd', 0, 'Newly created organization title listed.');
    $this->assertSession()->linkExists('Nameless #3', 0, 'Nameless organization title listed.');
    $this->assertSession()->pageTextContains('Supplier');

    // Assert all view headers are available.
    $this->assertSession()->linkExists('Name');
    $this->assertSession()->linkExists('Organization type');
    $this->assertSession()->linkExists('Updated');
    $this->assertSession()->pageTextContains('Operations');

    $labels = $this->xpath('//form[@class="views-exposed-form"]/div/label[text()="Type"]');
    $this->assertCount(1, $labels, 'Organization type is an exposed filter.');

    $labels = $this->xpath('//form[@class="views-exposed-form"]/div/label[text()="Name"]');
    $this->assertCount(1, $labels, 'Name is an exposed filter.');

    $labels = $this->xpath('//form[@class="views-exposed-form"]/div/label[text()="Active"]');
    $this->assertCount(1, $labels, 'Active is an exposed filter.');

    $organizations = \Drupal::entityTypeManager()
      ->getStorage('crm_core_organization')
      ->loadByProperties(['name' => 'Example ltd']);
    $organization = current($organizations);

    // Create another user.
    $new_user = $this->drupalCreateUser();

    // Test EntityOwnerTrait functions on organization.
    $this->assertEquals($organization->getOwnerId(), $user->id());
    $this->assertEquals($organization->getOwner()->id(), $user->id());
    $organization->setOwner($new_user);
    $this->assertEquals($organization->getOwnerId(), $new_user->id());
    $this->assertEquals($organization->getOwner()->id(), $new_user->id());
    $organization->setOwnerId($user->id());
    $this->assertEquals($organization->getOwnerId(), $user->id());
    $this->assertEquals($organization->getOwner()->id(), $user->id());

    // Test whether owner is loaded from database correctly.
    $organizations = \Drupal::entityTypeManager()
      ->getStorage('crm_core_organization')
      ->loadByProperties(['name' => 'Example ltd']);
    $organization = current($organizations);
    $this->assertEquals($organization->getOwnerId(), $user->id());
    $this->assertEquals($organization->getOwner()->id(), $user->id());

    // And then make sure the result is correct even with owner being someone
    // else than the user who was logged in while creating entity.
    $organization->setOwnerId($new_user->id());
    $organization->save();
    $organizations = \Drupal::entityTypeManager()
      ->getStorage('crm_core_organization')
      ->loadByProperties(['name' => 'Example ltd']);
    $organization = current($organizations);
    $this->drupalGet('crm-core/organization');

    $this->assertEquals($organization->getOwnerId(), $new_user->id());
    $this->assertEquals($organization->getOwner()->id(), $new_user->id());

    $this->assertSession()->responseContains('crm-core/organization/' . $organization->id() . '/edit');
    $this->assertSession()->responseContains('crm-core/organization/' . $organization->id() . '/delete');

    // Organization updated date is available.
    $this->assertSession()->pageTextContains($this->container
      ->get('date.formatter')
      ->format($organization->get('changed')->value, 'short')
    );

    // Edit operation.
    $supplier_values = [
      'name[0][value]' => 'Another Example ltd',
      'status[value]' => TRUE,
    ];
    $this->drupalGet('crm-core/organization/' . $organization->id() . '/edit');
    $this->submitForm($supplier_values, 'Save Supplier');

    // Assert we are viewing the entity.
    $this->assertSession()->addressEquals('crm-core/organization/' . $organization->id());
    $this->assertSession()->pageTextContains('Another Example ltd');

    $this->drupalGet('crm-core/organization/1/edit');
    // Local task "Delete" is available.
    $this->assertSession()->linkByHrefExists('crm-core/organization/1/delete');
    // Check listing page.
    $this->drupalGet('crm-core/organization');
    $this->assertSession()->linkExists('Another Example ltd', 0, 'Updated organization title listed.');

    // Delete organizations.
    $this->drupalGet('crm-core/organization/1/delete');
    $this->submitForm([], 'Delete');
    $this->drupalGet('crm-core/organization/2/delete');
    $this->submitForm([], 'Delete');
    $this->drupalGet('crm-core/organization/3/delete');
    $this->submitForm([], 'Delete');
    $this->assertSession()->addressEquals('crm-core/organization');
    $this->assertSession()->linkNotExists('Another Example ltd', 'Deleted organization title no more listed.');

    // Assert that there are no organizations.
    $this->assertSession()->pageTextContains('There are no organizations available.');
  }

  /**
   * Tests the organization type operations.
   *
   * User with permissions 'administer organization types' should be able to
   * create/edit/delete organization types.
   */
  public function testOrganizationTypeOperations() {
    // Create user with permission 'administer organization types'.
    $user = $this->drupalCreateUser(['administer organization types']);
    $this->drupalLogin($user);

    $this->drupalGet('admin/structure/crm-core/organization-types');

    // Test that there are edit, delete links for existing organizations.
    $this->assertOrganizationTypeLink('supplier', 'Edit link for supplier.');
    $this->assertOrganizationTypeLink('supplier/delete', 'Delete link for supplier.');

    $this->assertOrganizationTypeLink('household', 'Edit link for household.');
    $this->assertOrganizationTypeLink('household/delete', 'Delete link for household.');

    // Add another organization type.
    $second_organization_type = OrganizationType::create([
      'id' => 'new_organization_type',
      'label' => 'New organization type',
      'primary_fields' => [],
      'description' => 'New organization type description',
    ]);
    $second_organization_type->save();

    $this->drupalGet('admin/structure/crm-core/organization-types');

    // Create organization of type 'supplier.'.
    Organization::create(['name' => 'Supplier A', 'type' => 'supplier'])->save();

    $this->drupalGet('admin/structure/crm-core/organization-types');

    // Test that there is no a delete link.
    $this->assertNoOrganizationTypeLink('supplier/delete', 'No delete link for supplier.');
    $this->drupalGet('admin/structure/crm-core/organization-types/supplier/delete');
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalGet('admin/structure/crm-core/organization-types/supplier');

    // Test that there is no a delete link on supplier type form.
    $this->assertNoOrganizationTypeLink('supplier/delete', 'No delete link on supplier type form.');
  }

  /**
   * Test if the field UI is displayed on organization bundle.
   */
  public function testFieldsUi() {
    $user = $this->drupalCreateUser([
      'administer crm_core_organization display',
      'administer crm_core_organization form display',
      'administer crm_core_organization fields',
      'administer organization types',
    ]);
    $this->drupalLogin($user);

    // List of all types.
    $this->drupalGet('admin/structure/crm-core/organization-types');
    $this->assertSession()->linkExists('Edit');
    $this->assertSession()->linkExists('Manage fields');
    $this->assertSession()->linkExists('Manage form display');
    $this->assertSession()->linkExists('Manage display');

    // Edit on type.
    $this->drupalGet('admin/structure/crm-core/organization-types/supplier');
    $this->assertSession()->linkExists('Edit');
    $this->assertSession()->linkExists('Manage fields');
    $this->assertSession()->linkExists('Manage form display');
    $this->assertSession()->linkExists('Manage display');

    // Manage fields on type.
    $this->drupalGet('admin/structure/crm-core/organization-types/supplier/fields');
    $this->assertSession()->linkExists('Edit');
    $this->assertSession()->linkExists('Manage fields');
    $this->assertSession()->linkExists('Manage form display');
    $this->assertSession()->linkExists('Manage display');

    $this->drupalGet('admin/structure/crm-core/organization-types/supplier/form-display');
    $this->assertSession()->pageTextContains('Name');

    $this->drupalGet('admin/structure/crm-core/organization-types/supplier/display');
    $this->assertSession()->pageTextContains('Name');
  }

  /**
   * Test organization revisions.
   */
  public function testOrganizationRevisions() {
    $user = $this->drupalCreateUser([
      'administer crm_core_organization entities',
      'view all crm_core_organization revisions',
    ]);
    $this->drupalLogin($user);

    $organization = ['name[0][value]' => 'rev'];
    $this->drupalGet('crm-core/organization/add/supplier');
    $this->submitForm($organization, 'Save Supplier');

    $organization_1 = ['name[0][value]' => 'rev1'];
    $this->drupalGet('crm-core/organization/1/edit');
    $this->submitForm($organization_1, 'Save Supplier');

    $organization_2 = ['name[0][value]' => 'rev2'];
    $this->drupalGet('crm-core/organization/1/edit');
    $this->submitForm($organization_2, 'Save Supplier');

    $this->clickLink('Revisions');
    $this->assertSession()->linkByHrefExists('crm-core/organization/1');
    $this->assertSession()->linkByHrefExists('crm-core/organization/1/revisions/1/view');
    $this->assertSession()->linkByHrefExists('crm-core/organization/1/revisions/2/view');

    $this->drupalGet('crm-core/organization/1/revisions/1/view');
    $this->assertSession()->pageTextContains('rev');
    $this->drupalGet('crm-core/organization/1/revisions/2/view');
    $this->assertSession()->pageTextContains('rev1');
  }

  /**
   * Asserts an organization type link.
   *
   * The path 'admin/structure/crm-core/organization-types/' gets prepended to
   * the path provided.
   *
   * @see WebTestBase::assertLinkByHref()
   */
  public function assertOrganizationTypeLink($href, $message = '') {
    $this->assertSession()->linkByHrefExists('admin/structure/crm-core/organization-types/' . $href, 0, $message);
  }

  /**
   * Asserts no organization type link.
   *
   * The path 'admin/structure/crm-core/organization-types/' gets prepended to
   * the path provided.
   *
   * @see WebTestBase::assertNoLinkByHref()
   */
  public function assertNoOrganizationTypeLink($href, $message = '') {
    $this->assertSession()->linkByHrefNotExists('admin/structure/crm-core/organization-types/' . $href, $message);
  }

}
