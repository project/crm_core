<?php

namespace Drupal\Tests\crm_core_contact\Traits;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\crm_core_contact\Entity\OrganizationType;
use PHPUnit\Framework\TestCase;

/**
 * Provides methods to create content type from given values.
 *
 * This trait is meant to be used only by test classes.
 */
trait OrganizationTypeCreationTrait {

  /**
   * Creates a custom content type based on default settings.
   *
   * @param array $values
   *   An array of settings to change from the defaults.
   *   Example: 'type' => 'foo'.
   *
   * @return \Drupal\crm_core_contact\Entity\OrganizationType
   *   Created content type.
   */
  protected function createOrganizationType(array $values = []) {
    // Find a non-existent random type name.
    if (!isset($values['type'])) {
      do {
        $id = strtolower($this->randomMachineName(8));
      } while (OrganizationType::load($id));
    }
    else {
      $id = $values['type'];
    }
    $values += [
      'id' => $id,
      'name' => $id,
      'primary_fields' => [],
    ];
    $type = OrganizationType::create($values);
    $status = $type->save();

    if ($this instanceof TestCase) {
      $this->assertSame($status, SAVED_NEW, (new FormattableMarkup('Created content type %type.', ['%type' => $type->id()]))->__toString());
    }
    else {
      $this->assertEquals(SAVED_NEW, $status, (new FormattableMarkup('Created content type %type.', ['%type' => $type->id()]))->__toString());
    }

    return $type;
  }

}
