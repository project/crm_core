<?php

namespace Drupal\Tests\crm_core_contact\Kernel;

use Drupal\Core\Language\LanguageInterface;
use Drupal\KernelTests\KernelTestBase;

/**
 * Tests the admin listing fallback when views is not enabled.
 *
 * @group crm_core_contact
 */
class ContactListBuilderTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['crm_core_contact', 'name', 'user'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('crm_core_individual');
    $this->installEntitySchema('crm_core_organization');
  }

  /**
   * Tests that the correct cache contexts are set.
   */
  public function testIndividualCacheContexts() {
    /** @var \Drupal\Core\Entity\EntityListBuilderInterface $list_builder */
    $list_builder = $this->container->get('entity_type.manager')->getListBuilder('crm_core_individual');

    $build = $list_builder->render();
    $this->container->get('renderer')->renderRoot($build);

    $this->assertEqualsCanonicalizing([
      'languages:' . LanguageInterface::TYPE_INTERFACE,
      'theme',
      'url.query_args.pagers:0',
      'user.permissions',
    ], $build['#cache']['contexts']);
  }

  /**
   * Tests that the correct cache contexts are set.
   */
  public function testOrganizationCacheContexts() {
    /** @var \Drupal\Core\Entity\EntityListBuilderInterface $list_builder */
    $list_builder = $this->container->get('entity_type.manager')->getListBuilder('crm_core_organization');

    $build = $list_builder->render();
    $this->container->get('renderer')->renderRoot($build);

    $this->assertEqualsCanonicalizing([
      'languages:' . LanguageInterface::TYPE_INTERFACE,
      'theme',
      'url.query_args.pagers:0',
      'user.permissions',
    ], $build['#cache']['contexts']);
  }

}
