/**
 * @file
 */

(function mergeTableOnLoad($) {
  Drupal.behaviors.crm_core_contact_merge_table = {
    attach() {
      $('#merge-contacts-table tr').each(function rowIterator() {
        const subtext = $(this);
        const allRadios = $('input[type=radio]', subtext);
        $('input[type=radio]:not(.processed)', subtext).change(
          function radioChanged() {
            allRadios.addClass('processed');
            allRadios.not(this).attr('checked', '');
            allRadios.removeClass('processed');
          },
        );
      });
    },
  };
})(jQuery);
