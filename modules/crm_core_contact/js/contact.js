/**
 * drupalSettings for contact forms.
 * */

(function contactDetailOnLoad($, Drupal) {
  Drupal.behaviors.crmCoreContactDetailsSummaries = {
    attach: function attach(context) {
      const $context = $(context);
      $context
        .find('.contact-form-owner')
        .drupalSetSummary(function ownerSummary(subtext) {
          const $authorContext = $(subtext);
          const name = $authorContext.find('.field--name-uid input').val();
          const date = $authorContext.find('.field--name-created input').val();

          if (name && date) {
            return Drupal.t('By @name on @date', {
              '@name': name,
              '@date': date,
            });
          }

          if (name) {
            return Drupal.t('By @name', {
              '@name': name,
            });
          }

          if (date) {
            return Drupal.t('Created on @date', {
              '@date': date,
            });
          }
        });
      $context
        .find('.contact-form-options')
        .drupalSetSummary(function optionSummary(subtext) {
          const $optionsContext = $(subtext);
          const vals = [];

          if ($optionsContext.find('input').is(':checked')) {
            $optionsContext
              .find('input:checked')
              .next('label')
              .each(function optionIterator() {
                vals.push(Drupal.checkPlain($(this).text().trim()));
              });
            return vals.join(', ');
          }

          return Drupal.t('Not promoted');
        });
    },
  };
})(jQuery, Drupal);
