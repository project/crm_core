<?php

namespace Drupal\Tests\crm_core\Unit\Theme;

use Drupal\Core\Routing\RouteMatch;
use Drupal\crm_core\Theme\CrmCoreNegotiator;
use Drupal\Tests\UnitTestCase;
use Symfony\Component\Routing\Route;

/**
 * Tests CrmCoreNegotiator class.
 *
 * @group crm_core
 * @coversDefaultClass \Drupal\crm_core\Theme\CrmCoreNegotiator
 */
class CrmCoreNegotiatorTest extends UnitTestCase {

  /**
   * Tests for the Crm Core custom theme.
   *
   * @dataProvider getThemes
   */
  public function testDetermineActiveTheme($custom_theme, $expected) {

    $config_factory = $this->getConfigFactoryStub([
      'crm_core.settings' => ['custom_theme' => $custom_theme],
    ]);

    $route = $this->prophesize(Route::class);
    $route->getPath()->willReturn('/crm-core');
    $route_match = $this->prophesize(RouteMatch::class);
    $route_match->getRouteObject()->willReturn($route);

    $negotiator = new CrmCoreNegotiator($config_factory);
    $actual = $negotiator->determineActiveTheme($route_match->reveal());
    $this->assertSame($expected, $actual);
  }

  /**
   * Provides a list of theme names to test.
   */
  public static function getThemes() {
    return [
      ['seven', 'seven'],
      [NULL, NULL],
      ['', NULL],
    ];
  }

}
